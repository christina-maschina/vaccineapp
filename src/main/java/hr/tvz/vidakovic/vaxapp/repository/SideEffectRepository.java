package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.domain.SideEffect;

import java.util.List;
import java.util.Optional;

public interface SideEffectRepository {
    Optional<SideEffect> findSideEffectByShortDescription(String shortDescription);
    List<SideEffect> findAll();

}
