package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.domain.Vaccine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VaccineJpaRepository extends JpaRepository<Vaccine, Long> {
    List<Vaccine> findByAvailableDosesGreaterThanEqual(Long availableDoses);
}
