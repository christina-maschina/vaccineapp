package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.domain.Vaccine;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Primary
@Repository
public class JdbcVaccineRepository implements VaccineRepository {
    private final JdbcTemplate jdbc;
    private final SimpleJdbcInsert vaccineInserter;
    private final ModelMapper modelMapper;

    public JdbcVaccineRepository(JdbcTemplate jdbc, ModelMapper modelMapper) {
        this.jdbc = jdbc;
        this.vaccineInserter = new SimpleJdbcInsert(jdbc)
                .withTableName("Vaccine")
                .usingGeneratedKeyColumns("id");
        this.modelMapper = modelMapper;
    }

    @Override
    public List<Vaccine> findAll() {
        return jdbc.query("select * from Vaccine", this::mapRowToVaccine);
    }

    @Override
    public Optional<Vaccine> findVaccineByResearchName(String researchName) {
        try {
            return Optional.ofNullable(jdbc.queryForObject("select * from Vaccine where research_name=?",
                    this::mapRowToVaccine, researchName));
        } catch (EmptyResultDataAccessException ex) {
            return Optional.ofNullable(null);
        }
    }

    @Override
    public List<Vaccine> findVaccinesByRequiredDose(Long requiredDose) {
        return jdbc.query("select * from Vaccine where number_of_shots=?",
                this::mapRowToVaccine, requiredDose);
    }

    @Override
    public Optional<Vaccine> save(VaccineCommand command) {
        Optional<Vaccine> findExistingVaccine = findVaccineByResearchName(command.getResearchName());

        if (findExistingVaccine.isPresent()) {
            return Optional.ofNullable(null);
        }

        command.setId(saveVaccineDetails(command));
        Vaccine vaccine = modelMapper.map(command, Vaccine.class);
        return Optional.of(vaccine);
    }

    private Long saveVaccineDetails(VaccineCommand command) {
        Map<String, Object> values = new HashMap<>();
        values.put("research_name", command.getResearchName());
        values.put("manufacturer_name", command.getManufacturerName());
        values.put("type", command.getType());
        values.put("number_of_shots", command.getNumberOfShots());
        values.put("available_doses", command.getAvailableDoses());
        return vaccineInserter.executeAndReturnKey(values).longValue();
    }

    @Override
    public void delete(String researchName) {
        Optional<Vaccine> vaccine =findVaccineByResearchName(researchName);
        if(vaccine.isPresent()){
            jdbc.update("delete from Side_Effect where vaccine_id=?", vaccine.get().getId());
            jdbc.update("delete from Vaccine where research_name=?", researchName);
        }
    }

    @Override
    public Optional<Vaccine> update(String researchName, VaccineCommand command) {
        Optional<Vaccine> findExistingVaccine = findVaccineByResearchName(researchName);

        if (findExistingVaccine.isEmpty()) {
            return Optional.ofNullable(null);
        }
        delete(findExistingVaccine.get().getResearchName());
        return save(command);
    }

    @Override
    public List<Vaccine> findAllByAvailableDose(final long limit1, final long limit2) {
        return jdbc.query("select * from Vaccine where available_doses between ? and ?",
                this::mapRowToVaccine, limit1, limit2);
    }

    @Override
    public Optional<Vaccine> findById(long id) {
        try {
            return Optional.ofNullable(jdbc.queryForObject("select * from Vaccine where id = ?",
                    this::mapRowToVaccine, id));
        } catch (EmptyResultDataAccessException ex) {
            return Optional.ofNullable(null);
        }
    }

    private Vaccine mapRowToVaccine(ResultSet rs, int rowNum) throws SQLException {
        Vaccine vaccine = new Vaccine();
        vaccine.setId(rs.getLong("id"));
        vaccine.setResearchName(rs.getString("research_name"));
        vaccine.setManufacturerName(rs.getString("manufacturer_name"));
        vaccine.setType(rs.getString("type"));
        vaccine.setNumberOfShots(rs.getLong("number_of_shots"));
        vaccine.setAvailableDoses(rs.getLong("available_doses"));
        return vaccine;
    }
}
