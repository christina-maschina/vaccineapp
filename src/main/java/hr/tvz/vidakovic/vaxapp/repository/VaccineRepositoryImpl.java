package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.domain.Vaccine;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class VaccineRepositoryImpl implements VaccineRepository {

    private final ModelMapper modelMapper;

    private List<Vaccine> mocked_vaccine = new ArrayList<>(Arrays.asList(
            new Vaccine(1L,"AZD1222", "Astra Zeneca", "viral vector", 1L, 2000L),
            new Vaccine(2L, "BNT162b2", "Pfizer-BioNTech", "mRNA", 2L, 42000L),
            new Vaccine(3L,"mRNA-1273", "Moderna", "mRNA", 2L, 50000L),
            new Vaccine(4L,"JNJ-78436735", "Johnson & Johnson", "viral vector", 3L, 40000L),
            new Vaccine(5L,"BBIBP-CorV", "Sinopharm", "viral vector", 2L, 40000L)));

    public VaccineRepositoryImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    @Override
    public List<Vaccine> findAll() {
        return mocked_vaccine;
    }

    @Override
    public Optional<Vaccine> findVaccineByResearchName(final String researchName) {
        Optional<Vaccine> vaccineByName = mocked_vaccine
                .stream()
                .filter(vaccine -> vaccine.getResearchName().equals(researchName))
                .findAny();

        if (vaccineByName.isEmpty()) {
            return Optional.ofNullable(null);
        }

        return vaccineByName;
    }

    @Override
    public List<Vaccine> findVaccinesByRequiredDose(final Long requiredDose) {
        List<Vaccine> vaccines = mocked_vaccine
                .stream()
                .filter(vaccine -> vaccine.getAvailableDoses() >= requiredDose)
                .collect(Collectors.toList());

        return vaccines;
    }

    @Override
    public Optional<Vaccine> save(final VaccineCommand command) {
        Optional<Vaccine> findExistingVaccine = findVaccineByResearchName(command.getResearchName());

        if (findExistingVaccine.isPresent()) {
            return Optional.ofNullable(null);
        }

        Vaccine vaccine = modelMapper.map(command, Vaccine.class);
        mocked_vaccine.add(vaccine);

        return Optional.of(vaccine);

    }

    @Override
    public void delete(final String researchName) {
        Vaccine vaccineToDelete = findVaccineByResearchName(researchName).get();
        mocked_vaccine.remove(vaccineToDelete);

    }

    @Override
    public Optional<Vaccine> update(final String researchName, final VaccineCommand command) {
        Optional<Vaccine> findExistingVaccine = findVaccineByResearchName(researchName);

        if (findExistingVaccine.isEmpty()) {
            return Optional.ofNullable(null);
        }

        mocked_vaccine.remove(findExistingVaccine.get());
        Vaccine vaccine = modelMapper.map(command, Vaccine.class);
        mocked_vaccine.add(vaccine);

        return Optional.of(vaccine);

    }

    @Override
    public List<Vaccine> findAllByAvailableDose(long param1, long param2) {
        return null;
    }

    @Override
    public Optional<Vaccine> findById(long id) {
        return null;
    }

}
