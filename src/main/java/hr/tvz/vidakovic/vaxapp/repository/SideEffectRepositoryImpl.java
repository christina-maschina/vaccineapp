package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.domain.SideEffect;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Repository
public class SideEffectRepositoryImpl implements SideEffectRepository {

    private List<SideEffect> moccked_sideEffects = new ArrayList<>(Arrays.asList(
            new SideEffect(1L, "Temperatura", "Moguća je pojava alergijske reakcije na određene sastojke cjepiva", 2),
            new SideEffect(2L, "Glavobolja", "Neka druga reakcija opis", 2),
            new SideEffect(3L, "Crvenilo", "Neka treća reakcija opis", 5),
            new SideEffect(4L, "Alergija", "Neka četvrta reakcija opis", 3),
            new SideEffect(5L, "Slabost", "Neka peta reakcija opis", 1),
            new SideEffect(6L, "Umor", "Neka šesta reakcija opis", 10),
            new SideEffect(7L, "Smrtnost", "Neka sedma reakcija opis", 1)));
    @Override
    public List<SideEffect> findAll() {
        return moccked_sideEffects;
    }


    @Override
    public Optional<SideEffect> findSideEffectByShortDescription(String shortDescription) {
        Optional<SideEffect> effectByShortDescr = moccked_sideEffects
                .stream()
                .filter(effect -> effect.getShortDescription().equals(shortDescription))
                .findAny();

        if (effectByShortDescr.isEmpty()) {
            return Optional.ofNullable(null);
        }

        return effectByShortDescr;
    }
}
