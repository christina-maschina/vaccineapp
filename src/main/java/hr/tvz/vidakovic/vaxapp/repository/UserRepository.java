package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByUsername(String username);
}
