package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.domain.SideEffect;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SideEffectJpaRepository extends JpaRepository<SideEffect, Long> {
    List<SideEffect> findAll();
    List<SideEffect> findByVaccine_ResearchName(String researchName);
    List<SideEffect> findByDescriptionLike(String description);
    Optional<SideEffect> findByShortDescription(String shortDescription);
}
