package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.domain.Vaccine;

import java.util.List;
import java.util.Optional;

public interface VaccineRepository {
    List<Vaccine> findAll();
    Optional<Vaccine> findVaccineByResearchName(String researchName);
    List<Vaccine> findVaccinesByRequiredDose(Long requiredDose);
    Optional<Vaccine> save(VaccineCommand command);
    void delete(String researchName);
    Optional<Vaccine> update(String researchName, VaccineCommand command);
    List<Vaccine> findAllByAvailableDose(long limit1, long limit2);
    Optional<Vaccine> findById(long id);
}
