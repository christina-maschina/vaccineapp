package hr.tvz.vidakovic.vaxapp.controller;

import hr.tvz.vidakovic.vaxapp.service.SideEffectService;
import hr.tvz.vidakovic.vaxapp.transfer.SideEffectDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/side-effect", produces = "application/json")
@CrossOrigin("http://localhost:4200")
public class SideEffectController {

    private final SideEffectService sideEffectService;

    public SideEffectController(SideEffectService sideEffectService) {
        this.sideEffectService = sideEffectService;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping
    public List<SideEffectDTO> getAll() {
        return sideEffectService.findAll();
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping(params = "vaccineResearchName")
    public ResponseEntity<List<SideEffectDTO>> getByResearchName(@RequestParam final String vaccineResearchName) {
        return new ResponseEntity<>(sideEffectService.findByResearchName(vaccineResearchName), HttpStatus.OK);
    }

    @GetMapping("get/{shortDescription}")
    public ResponseEntity<SideEffectDTO> getByShortDescription(@PathVariable final String shortDescription) {
        return sideEffectService.findSideEffectByShortDescription(shortDescription)
                .map(sideEffectDTO -> ResponseEntity
                        .status(HttpStatus.OK)
                        .body(sideEffectDTO))
                .orElseGet(() -> ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .build()
                );
    }

    @GetMapping("/search/{longDescription}")
    public ResponseEntity<List<SideEffectDTO>> findByLongDescription(@PathVariable final String longDescription) {
        return new ResponseEntity<>(sideEffectService.findByLongDescription("%" + longDescription + "%"), HttpStatus.OK);
    }

}
