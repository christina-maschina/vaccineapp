package hr.tvz.vidakovic.vaxapp.controller;

import hr.tvz.vidakovic.vaxapp.service.UserService;
import hr.tvz.vidakovic.vaxapp.transfer.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import hr.tvz.vidakovic.vaxapp.security.SecurityUtils;

import java.util.Optional;

@RestController
@RequestMapping(value = "api/user", produces = "application/json")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/current-user")
    public ResponseEntity<UserDTO> getCurrentUser() {
        System.out.println(SecurityUtils.getCurrentUserJWT());

        Optional<String> username = SecurityUtils.getCurrentUserUsername();

        return userService.findByUsername(username.get())
                .map(userDTO -> ResponseEntity
                        .status(HttpStatus.OK)
                        .body(userDTO))
                .orElseGet(() -> ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .build());
    }
}
