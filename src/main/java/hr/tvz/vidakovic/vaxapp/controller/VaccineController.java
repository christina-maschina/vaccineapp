package hr.tvz.vidakovic.vaxapp.controller;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.service.VaccineService;
import hr.tvz.vidakovic.vaxapp.transfer.VaccineDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/vaccine", produces = "application/json")
@CrossOrigin("http://localhost:4200")
public class VaccineController {
    private final VaccineService vaccineService;

    public VaccineController(VaccineService vaccineService) {
        this.vaccineService = vaccineService;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER", "ROLE_UPDATER"})
    @GetMapping
    public List<VaccineDTO> getAll() {
        return vaccineService.findAll();
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/{researchName}")
    public ResponseEntity<VaccineDTO> getByName(@PathVariable final String researchName) {
        return vaccineService.findVaccineByResearchName(researchName)
                .map(vaccineDTO -> ResponseEntity
                        .status(HttpStatus.OK)
                        .body(vaccineDTO))
                .orElseGet(() -> ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .build()
                );
    }

    @RequestMapping(params = "requiredDose")
    public List<VaccineDTO> getByNumberOfRequiredDose(@RequestParam final Long requiredDose) {
        return vaccineService.findVaccinesByRequiredDose(requiredDose);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    public ResponseEntity<VaccineDTO> save(@Valid @RequestBody final VaccineCommand command) {
        return vaccineService.save(command)
                .map(vaccineDTO -> ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(vaccineDTO)
                )
                .orElseGet(
                        () -> ResponseEntity
                                .status(HttpStatus.CONFLICT)
                                .build()
                );
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{researchName}")
    public ResponseEntity delete(@PathVariable final String researchName) {
        vaccineService.delete(researchName);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Secured({"ROLE_ADMIN", "ROLE_UPDATER"})
    @PutMapping("/{researchName}")
    public ResponseEntity<VaccineDTO> update(@PathVariable final String researchName, @RequestBody @Valid final VaccineCommand command) {
        return vaccineService.update(researchName, command)
                .map(vaccineDTO -> ResponseEntity
                        .status(HttpStatus.OK)
                        .body(vaccineDTO)
                )
                .orElseGet(
                        () -> ResponseEntity
                                .status(HttpStatus.NOT_FOUND)
                                .build()
                );
    }

    @GetMapping(params = {"lowerLimit", "upperLimit"})
    public List<VaccineDTO> findAllByAvailableDose(@RequestParam("lowerLimit") final long limit1, @RequestParam("upperLimit") final long limit2){
        return vaccineService.findAllByAvailableDose(limit1, limit2);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<VaccineDTO>getById(@PathVariable final long id){
        return vaccineService.findById(id)
                .map(vaccineDTO -> ResponseEntity
                        .status(HttpStatus.OK)
                        .body(vaccineDTO)
                )
                .orElseGet(
                        () -> ResponseEntity
                                .status(HttpStatus.NOT_FOUND)
                                .build()
                );
    }

}
