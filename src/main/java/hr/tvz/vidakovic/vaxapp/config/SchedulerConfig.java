package hr.tvz.vidakovic.vaxapp.config;

import hr.tvz.vidakovic.vaxapp.domain.VaccineJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SchedulerConfig {

    @Bean
    JobDetail vaccineJobDetail(){
        return JobBuilder.newJob(VaccineJob.class).withIdentity("vaccineJob").storeDurably().build();
    }

    @Bean
    public Trigger vaccineJobTrigger(){
        SimpleScheduleBuilder scheduleBuilder=SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(10).repeatForever();

        return TriggerBuilder.newTrigger().forJob(vaccineJobDetail())
                .withIdentity("vaccineTrigger").withSchedule(scheduleBuilder).build();
    }

    @Bean
    public Trigger vaccineJobTriggerMidnight(){
        return TriggerBuilder.newTrigger().forJob(vaccineJobDetail())
                .withIdentity("vaccineTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 0 ? * 1,7"))
                .build();
    }


}
