package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.domain.Vaccine;
import hr.tvz.vidakovic.vaxapp.repository.VaccineJpaRepository;
import hr.tvz.vidakovic.vaxapp.repository.VaccineRepository;
import hr.tvz.vidakovic.vaxapp.transfer.VaccineDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VaccineServiceImpl implements VaccineService {
    private final VaccineRepository vaccineRepository;
    private final VaccineJpaRepository vaccineJpaRepository;
    private final ModelMapper modelMapper;

    public VaccineServiceImpl(VaccineRepository vaccineRepository, VaccineJpaRepository vaccineJpaRepository, ModelMapper modelMapper) {
        this.vaccineRepository = vaccineRepository;
        this.vaccineJpaRepository = vaccineJpaRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<VaccineDTO> findAll() {
        return vaccineRepository.findAll()
                .stream()
                .map(vaccine -> modelMapper.map(vaccine, VaccineDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<VaccineDTO> findVaccineByResearchName(final String researchName) {
        Optional<Vaccine> vaccineOptional = vaccineRepository.findVaccineByResearchName(researchName);

        if (vaccineOptional.isEmpty()) {
            return Optional.ofNullable(null);
        }

        return mapVaccineToDTOOptional(vaccineOptional);

    }

    @Override
    public List<VaccineDTO> findVaccinesByRequiredDose(final Long requiredDose) {
        return vaccineRepository.findVaccinesByRequiredDose(requiredDose)
                .stream()
                .map(vaccine -> modelMapper.map(vaccine, VaccineDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<VaccineDTO> save(final VaccineCommand command) {
        Optional<Vaccine> vaccineOptional = vaccineRepository.save(command);

        if (vaccineOptional.isEmpty()) {
            return Optional.ofNullable(null);
        }

        return mapVaccineToDTOOptional(vaccineOptional);
    }

    @Override
    public void delete(final String researchName) {
        vaccineRepository.delete(researchName);
    }

    @Override
    public Optional<VaccineDTO> update(final String researchName, final VaccineCommand command) {
        Optional<Vaccine> vaccineOptional = vaccineRepository.update(researchName, command);

        if (vaccineOptional.isEmpty()) {
            return Optional.ofNullable(null);
        }

        return mapVaccineToDTOOptional(vaccineOptional);
    }

    @Override
    public List<VaccineDTO> findAllByAvailableDose(final long limit1, final long limit2) {
        return vaccineRepository.findAllByAvailableDose(limit1, limit2)
                .stream()
                .map(vaccine -> modelMapper.map(vaccine, VaccineDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<VaccineDTO> findById(long id) {
        Optional<Vaccine> vaccineOptional = vaccineRepository.findById(id);

        if (vaccineOptional.isEmpty()) {
            return Optional.ofNullable(null);
        }

        return mapVaccineToDTOOptional(vaccineOptional);
    }

    @Override
    public List<VaccineDTO> findByAvailableDoses(Long availableDoses) {
        List<Vaccine> vaccines = vaccineJpaRepository.findByAvailableDosesGreaterThanEqual(availableDoses);
        return vaccines
                .stream()
                .map(vaccine -> modelMapper.map(vaccine, VaccineDTO.class))
                .collect(Collectors.toList());
    }

    private Optional<VaccineDTO> mapVaccineToDTOOptional(Optional<Vaccine> vaccine) {
        return Optional.of(
                new VaccineDTO(vaccine.get().getResearchName(), vaccine.get().getManufacturerName(), vaccine.get().getType(),
                        vaccine.get().getNumberOfShots(), vaccine.get().getAvailableDoses()));

    }
}
