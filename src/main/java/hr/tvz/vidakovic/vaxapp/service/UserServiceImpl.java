package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.domain.Authority;
import hr.tvz.vidakovic.vaxapp.domain.SideEffect;
import hr.tvz.vidakovic.vaxapp.domain.User;
import hr.tvz.vidakovic.vaxapp.repository.UserRepository;
import hr.tvz.vidakovic.vaxapp.transfer.SideEffectDTO;
import hr.tvz.vidakovic.vaxapp.transfer.UserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepo;
    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepo, ModelMapper modelMapper) {
        this.userRepo = userRepo;
        this.modelMapper = modelMapper;
    }

    @Override
    public Optional<UserDTO> findByUsername(String username) {
         Optional<User> userOptional= userRepo.findOneByUsername(username);
         if(userOptional.isEmpty()){
             return Optional.ofNullable(null);
         }
         return mapOptionalToOptionalDTO(userOptional);
    }

    private Optional<UserDTO> mapOptionalToOptionalDTO(Optional<User> userOptional) {

        return Optional.of(
                new UserDTO(userOptional.get().getId(), userOptional.get().getUsername(), userOptional.get().getFirstName(),
                        userOptional.get().getLastName(),
                        userOptional.get().getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet())));

    }
}
