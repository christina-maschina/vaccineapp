package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.transfer.SideEffectDTO;

import java.util.List;
import java.util.Optional;

public interface SideEffectService {
    List<SideEffectDTO> findAll();
    Optional<SideEffectDTO> findSideEffectByShortDescription(String shortDescription);
    List<SideEffectDTO> findByResearchName(String researchName);
    List<SideEffectDTO> findByLongDescription(String longDescription);
    //Optional<SideEffectDTO> findByShortDescription(String shortDescription);

}
