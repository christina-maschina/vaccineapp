package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.domain.SideEffect;
import hr.tvz.vidakovic.vaxapp.repository.SideEffectJpaRepository;
import hr.tvz.vidakovic.vaxapp.repository.SideEffectRepository;
import hr.tvz.vidakovic.vaxapp.transfer.SideEffectDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SideEffectServiceImpl implements SideEffectService {

    private final ModelMapper modelMapper;
    private final SideEffectRepository repo;
    private final SideEffectJpaRepository sideEffectJpaRepo;

    public SideEffectServiceImpl(ModelMapper modelMapper, SideEffectRepository repo, SideEffectJpaRepository sideEffectJpaRepo) {
        this.modelMapper = modelMapper;
        this.repo = repo;
        this.sideEffectJpaRepo = sideEffectJpaRepo;
    }

    @Override
    public List<SideEffectDTO> findAll() {
        return sideEffectJpaRepo.findAll()
                .stream()
                .map(sideEffect -> modelMapper.map(sideEffect, SideEffectDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<SideEffectDTO> findByResearchName(String researchName) {
       return sideEffectJpaRepo.findByVaccine_ResearchName(researchName)
                .stream()
                .map(sideEffect -> modelMapper.map(sideEffect, SideEffectDTO.class))
                .collect(Collectors.toList());
    }


    @Override
    public List<SideEffectDTO> findByLongDescription(String longDescription) {
        List<SideEffect> sideEffects=  sideEffectJpaRepo.findByDescriptionLike(longDescription);
        List<SideEffectDTO> sideEffectDTOS = new ArrayList<>();
        for(SideEffect ef: sideEffects){
            SideEffectDTO sideEffectDTO;
            sideEffectDTO=modelMapper.map(ef, SideEffectDTO.class);
            sideEffectDTOS.add(sideEffectDTO);
        }
              return sideEffectDTOS;
    }

   /* @Override
    public Optional<SideEffectDTO> findByShortDescription(String shortDescription) {
         Optional <SideEffect> sideEffectOptional=sideEffectJpaRepo.findByShortDescription(shortDescription);
         if(sideEffectOptional.isEmpty()){
             return Optional.ofNullable(null);
         }
         return mapSideEffectOptionalToDTOOptional(sideEffectOptional);
    }*/


    @Override
    public Optional<SideEffectDTO> findSideEffectByShortDescription(String shortDescription) {
        Optional<SideEffect> sideEffectOptional = sideEffectJpaRepo.findByShortDescription(shortDescription);

        if (sideEffectOptional.isEmpty()) {
            return Optional.ofNullable(null);
        }
        return mapSideEffectOptionalToDTOOptional(sideEffectOptional);
    }

    private Optional<SideEffectDTO> mapSideEffectOptionalToDTOOptional(Optional<SideEffect> sideEffect) {
        return Optional.of(
                new SideEffectDTO(sideEffect.get().getShortDescription(), sideEffect.get().getDescription(), sideEffect.get().getFrequency()));

    }

}
