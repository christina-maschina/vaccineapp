package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.transfer.VaccineDTO;

import java.util.List;
import java.util.Optional;

public interface VaccineService {
    List<VaccineDTO> findAll();
    Optional<VaccineDTO> findVaccineByResearchName(String researchName);
    List<VaccineDTO> findVaccinesByRequiredDose(Long requiredDose);
    Optional<VaccineDTO> save(VaccineCommand command);
    void delete(String researchName);
    Optional<VaccineDTO> update(String researchName, VaccineCommand command);
    List<VaccineDTO> findAllByAvailableDose(long limit1, long limit2);
    Optional<VaccineDTO> findById(long id);
    List<VaccineDTO> findByAvailableDoses(Long availableDoses);
}
