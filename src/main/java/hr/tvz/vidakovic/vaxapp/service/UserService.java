package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.transfer.UserDTO;

import java.util.Optional;

public interface UserService {
    Optional<UserDTO> findByUsername(String username);
}
