package hr.tvz.vidakovic.vaxapp.command;

import lombok.*;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
public class VaccineCommand {

    private Long id;

    @NotBlank(message = "Research name must not be empty")
    private String researchName;

    @NotBlank(message = "Manufacturer name must not be empty")
    private String manufacturerName;

    //novo
    @NotBlank(message = "Type must be set to mRNA or VIRAL_VECTOR")
    private String type;

    //novo
    @PositiveOrZero(message = "Required doses must be a positive or zero number")
    private Long numberOfShots;

    //novo
    @Positive(message = "Available doses must be a positive number")
    private Long availableDoses;

    public VaccineCommand() {
    }

    public VaccineCommand(long id, String testResearchName, String testManufacturerName, String testType, long numberOfShots) {
    }
}
