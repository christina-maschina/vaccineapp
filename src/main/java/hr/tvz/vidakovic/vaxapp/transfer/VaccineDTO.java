package hr.tvz.vidakovic.vaxapp.transfer;

import lombok.*;


@AllArgsConstructor
@Data
@NoArgsConstructor
public class VaccineDTO {

    private String researchName;
    private String manufacturerName;
    private String type;
    private Long numberOfShots;
    private Long availableDoses;

}
