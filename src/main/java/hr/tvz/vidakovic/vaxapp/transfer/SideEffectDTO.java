package hr.tvz.vidakovic.vaxapp.transfer;

import lombok.*;


@AllArgsConstructor
@Data
@NoArgsConstructor
public class SideEffectDTO {

    private String shortDescription;
    private String description;
    private Integer frequency;

}
