package hr.tvz.vidakovic.vaxapp.domain;

import hr.tvz.vidakovic.vaxapp.service.VaccineService;
import hr.tvz.vidakovic.vaxapp.transfer.VaccineDTO;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;

public class VaccineJob extends QuartzJobBean {
    @Autowired
    private VaccineService vaccineService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<VaccineDTO> vaccineDTOS = vaccineService.findByAvailableDoses(1l);
        System.out.println("Ovo su trenutno dostupna cjepiva");
        System.out.println("---------------------------------");
        vaccineDTOS
                .stream()
                .forEach(vaccineDTO -> System.out.println(
                        vaccineDTO.getManufacturerName() + " - " + vaccineDTO.getAvailableDoses()
                ));
        System.out.println("---------------------------------");
    }
}
