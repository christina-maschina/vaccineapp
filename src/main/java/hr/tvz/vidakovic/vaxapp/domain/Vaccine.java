package hr.tvz.vidakovic.vaxapp.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table
public class Vaccine implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String researchName;
    private String manufacturerName;
    private String type;
    private Long numberOfShots;
    private Long availableDoses;

    @OneToMany(mappedBy = "vaccine")
    private Set<SideEffect> sideEffects = new HashSet<>();

    public Vaccine(Long id, String researchName, String manufacturerName, String vaccineType, Long requiredDose, Long availableDose ) {
        this.id = id;
        this.researchName = researchName;
        this.manufacturerName = manufacturerName;
        this.type = vaccineType;
        this.numberOfShots = requiredDose;
        this.availableDoses = availableDose;
    }
}
