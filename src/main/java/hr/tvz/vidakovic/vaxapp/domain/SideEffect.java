package hr.tvz.vidakovic.vaxapp.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@NoArgsConstructor
@Data
@Entity
@Table
public class SideEffect implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String shortDescription;
    private String description;
    private Integer frequency;

    @ManyToOne
    @JoinColumn(name = "vaccine_id")
    private Vaccine vaccine;

    public SideEffect(Long id, String shortDescription, String longDescription, Integer frequency) {
        this.id = id;
        this.shortDescription = shortDescription;
        this.description = longDescription;
        this.frequency = frequency;
    }

}
