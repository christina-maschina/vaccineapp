delete from Side_Effect;
delete from Vaccine;
delete from user_authority;
delete from user;
delete from authority;

INSERT INTO Vaccine VALUES (1, 'AZD1222', 'Astra Zeneca', 'viral vector', 1, 2000);
INSERT INTO Vaccine VALUES (2, 'BNT162b2', 'Pfizer-BioNTech', 'mRNA', 2, 42000);
INSERT INTO Vaccine VALUES (3, 'mRNA-1273', 'Moderna', 'mRNA', 2, 50000);
INSERT INTO Vaccine VALUES (4, 'JNJ-78436735', 'Johnson & Johnson', 'viral vector', 3, 40000);
INSERT INTO Vaccine VALUES (5, 'BBIBP-CorV', 'Sinopharm', 'viral vector', 2, 40000);

INSERT INTO Side_Effect (short_description, description, frequency, vaccine_id)
VALUES ('Temperatura', 'Moguća je pojava alergijske reakcije na određene sastojke cjepiva', 2, 4),
       ('Glavobolja', 'Neka druga reakcija opis', 2, 3),
       ('Crvenilo', 'Neka treca reakcija opis', 5, 2),
       ('Alergija', 'Neka cetvrta reakcija opis', 3, 2),
       ('Slabost', 'Neka peta reakcija opis', 1, 1),
       ('Umor', 'Neka sesta reakcija opis', 10, 5),
       ('Smrtnost', 'Neka sedma reakcija opis', 1, 5);

INSERT INTO user (id, username, password, first_name, last_name)
VALUES (1, 'admin', '$2y$12$FxT7y4KQw1h12CRN31c9Keeh3O.hKS0BBjtx61lEeu3OivxMUR1i6', 'Kristina', 'Vidakovic'),
       (2, 'user', '$2y$12$f43.k28J9AqIrVtNti/khuTwYJxxl.Kz2rUpM1XFnD00y49vUInGi', 'Matija', 'Dujmovic'),
       (3, 'newUser','$2y$12$S2qUS4OP3EYx0NJKOtyMEen64E0SqnaLqN203d6oriCB94UMz6j8K', 'Aleksander', 'Radovan');

INSERT INTO authority (id, name)
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER'),
       (3, 'ROLE_UPDATER');

INSERT INTO user_authority (user_id, authority_id)
VALUES  (1, 1),
        (2, 2),
        (3, 3);

