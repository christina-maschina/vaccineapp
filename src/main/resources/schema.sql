CREATE TABLE IF NOT EXISTS Vaccine
(
    id                IDENTITY PRIMARY KEY NOT NULL,
    research_name     VARCHAR(255)         NOT NULL,
    manufacturer_name VARCHAR(255)         NOT NULL,
    type              VARCHAR(55)          NOT NULL,
    number_of_shots   BIGINT               NOT NULL,
    available_doses   BIGINT               NOT NULL
);
CREATE TABLE IF NOT EXISTS Side_Effect
(
    id                IDENTITY PRIMARY KEY    NOT NULL,
    short_description VARCHAR(255)            NOT NULL,
    description  VARCHAR_IGNORECASE(255) NOT NULL,
    frequency         BIGINT                  NOT NULL,
    vaccine_id        BIGINT                  NOT NULL,
    FOREIGN KEY (vaccine_id) REFERENCES Vaccine (id)
);
CREATE TABLE IF NOT EXISTS user
(
    id         IDENTITY     NOT NULL,
    username   VARCHAR(50)  NOT NULL,
    password   VARCHAR(255) NOT NULL,
    first_name VARCHAR(50)  NOT NULL,
    last_name  VARCHAR(50)  NOT NULL
);
CREATE TABLE IF NOT EXISTS authority
(
    id   IDENTITY    NOT NULL,
    name VARCHAR(25) NOT NULL
);
CREATE TABLE IF NOT EXISTS user_authority
(
    user_id      BIGINT NOT NULL,
    authority_id BIGINT NOT NULL,
    PRIMARY KEY (user_id, authority_id),
    FOREIGN KEY (user_id) REFERENCES user (id),
    FOREIGN KEY (authority_id) REFERENCES authority (id)
);