package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.domain.Vaccine;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class VaccineRepositoryTest {

    @Autowired
    VaccineRepository vaccineRepository;
    VaccineCommand vaccineCommand;

    @Test
    void findAll() {
        List<Vaccine> vaccines = vaccineRepository.findAll();
        assertNotNull(vaccines);
        assertEquals("AZD1222", vaccines.get(0).getResearchName());
    }

    @Test
    void findVaccineByResearchName() {
        Optional<Vaccine> vaccine = vaccineRepository.findVaccineByResearchName("AZD1222");
        assertNotNull(vaccine);
        assertEquals(vaccine.get().getResearchName(), "AZD1222");
    }

    @Test
    void findVaccinesByRequiredDose() {
        List<Vaccine> vaccines = vaccineRepository.findVaccinesByRequiredDose(1L);
        assertNotNull(vaccines);
    }

    @Transactional
    @DirtiesContext
    @Test
    void save() {
        vaccineCommand = new VaccineCommand(7L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L);
        vaccineRepository.save(vaccineCommand);
        Optional<Vaccine> vaccine = vaccineRepository.findVaccineByResearchName("testResearchName");
        assertNotNull(vaccine);

    }

    @Transactional
    @DirtiesContext
    @Test
    void delete() {
        vaccineRepository.delete("AZD1222");
        Optional<Vaccine> vaccine = vaccineRepository.findVaccineByResearchName("AZD1222");
        assertTrue(vaccine.isEmpty());
    }

    @Transactional
    @DirtiesContext
    @Test
    void update() {
        vaccineCommand = new VaccineCommand(1L, "AZD1222", "Astra Zeneca", "viral vector", 2L, 2000L);
        vaccineRepository.update("AZD1222", vaccineCommand);
        Optional<Vaccine> vaccine = vaccineRepository.findVaccineByResearchName("AZD1222");
        assertEquals(vaccine.get().getNumberOfShots(), 2L);
    }

    @Test
    void findAllByAvailableDose() {
        List<Vaccine> vaccines=vaccineRepository.findAllByAvailableDose(2000, 40000);
        assertEquals(vaccines.size(), 3);
    }

    @Test
    void findById() {
        Optional<Vaccine> vaccine=vaccineRepository.findById(4);
        assertNotNull(vaccine);
        assertEquals(vaccine.get().getResearchName(), "JNJ-78436735");
        assertNotEquals(vaccine.get().getResearchName(), "BBIBP-CorV");
    }
}