package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    void findOneByUsername() {
        Optional<User> user=userRepository.findOneByUsername("user");
        assertNotNull(user);
        Optional<User> admin=userRepository.findOneByUsername("admin");
        assertNotNull(admin);
        Optional<User> newUser=userRepository.findOneByUsername("newUser");
        assertNotNull(newUser);
    }
}