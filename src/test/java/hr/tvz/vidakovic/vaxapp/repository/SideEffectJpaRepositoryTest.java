package hr.tvz.vidakovic.vaxapp.repository;

import hr.tvz.vidakovic.vaxapp.domain.SideEffect;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SideEffectJpaRepositoryTest {

    @Autowired
    SideEffectJpaRepository sideEffectJpaRepository;

    @Test
    void findAll() {
        List<SideEffect> sideEffects = sideEffectJpaRepository.findAll();
        assertNotNull(sideEffects);
        assertEquals(sideEffects.size(), 7);
    }

    @Test
    void findByVaccine_ResearchName() {
        List<SideEffect> sideEffects_pfizer = sideEffectJpaRepository.findByVaccine_ResearchName("BNT162b2");
        assertNotNull(sideEffects_pfizer);
        assertEquals(sideEffects_pfizer.size(), 2);
    }

    @Test
    void findByDescriptionLike() {
        List<SideEffect> sideEffects = sideEffectJpaRepository.findByDescriptionLike("%treca%");
        assertNotNull(sideEffects);
        assertEquals(sideEffects.size(), 1);
    }

    @Test
    void findByShortDescription() {
        Optional<SideEffect> sideEffects = sideEffectJpaRepository.findByShortDescription("Crvenilo");
        assertNotNull(sideEffects);
    }
}