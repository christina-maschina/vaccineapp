package hr.tvz.vidakovic.vaxapp.controller;

import hr.tvz.vidakovic.vaxapp.service.UserService;
import hr.tvz.vidakovic.vaxapp.transfer.UserDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.HashSet;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest{

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void getCurrentUser() throws Exception {
        when(userService.findByUsername(anyString())).thenReturn(
                java.util.Optional.of(new UserDTO(1L, "testUsername", "testFirstName", "testLastName",
                        new HashSet<>(Arrays.asList("TEST")))));

        this.mockMvc.perform(
                get("/api/user/current-user")
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(
                                user("newUser").password("newUser").roles("UPDATER")
                        )
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.username").value("testUsername"));
    }
}