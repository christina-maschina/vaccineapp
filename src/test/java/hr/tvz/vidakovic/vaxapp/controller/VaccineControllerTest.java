package hr.tvz.vidakovic.vaxapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.service.VaccineService;
import hr.tvz.vidakovic.vaxapp.transfer.SideEffectDTO;
import hr.tvz.vidakovic.vaxapp.transfer.VaccineDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class VaccineControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    VaccineService vaccineService;

    ObjectMapper objectMapper = new ObjectMapper();
    VaccineDTO vaccineDTO = new VaccineDTO("testResearchName", "testManufacturerName", "testType", 2L, 9000L);
    VaccineCommand vaccineCommand = new VaccineCommand(1L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L);

    void getAll() throws Exception {
        when(vaccineService.findAll()).thenReturn(
                new ArrayList<>(Arrays.asList(vaccineDTO)));

        this.mockMvc.perform(
                get("/vaccine")
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(
                                user("newUser").password("newUser").roles("UPDATER")
                        )
                        .with(csrf())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].researchName").value("testResearchName"))
                .andExpect(jsonPath("$[0].manufacturerName").value("testManufacturerName"))
                .andExpect(jsonPath("$[0].type").value("testType"))
                .andExpect(jsonPath("$[0].numberOfShots").value(2l))
                .andExpect(jsonPath("$[0].availableDoses").value(9000l));
    }

    @Test
    void getByName() throws Exception {
        when(vaccineService.findVaccineByResearchName(anyString())).thenReturn(Optional.ofNullable(vaccineDTO));

        this.mockMvc.perform(
                get("/vaccine/{researchName}", "testResearchName")
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(csrf())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.researchName").value("testResearchName"))
                .andExpect(jsonPath("$.manufacturerName").value("testManufacturerName"))
                .andExpect(jsonPath("$.type").value("testType"))
                .andExpect(jsonPath("$.numberOfShots").value(2l))
                .andExpect(jsonPath("$.availableDoses").value(9000l));
    }

    @Test
    void getByNumberOfRequiredDose() throws Exception {
        when(vaccineService.findVaccinesByRequiredDose(2l)).thenReturn(
                new ArrayList<>(Arrays.asList(vaccineDTO)));

        this.mockMvc.perform(
                get("/vaccine")
                        .param("requiredDose", String.valueOf(2l))
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(
                                user("newUser").password("newUser").roles("UPDATER")
                        )
                        .with(csrf())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].researchName").value("testResearchName"))
                .andExpect(jsonPath("$[0].manufacturerName").value("testManufacturerName"))
                .andExpect(jsonPath("$[0].type").value("testType"))
                .andExpect(jsonPath("$[0].numberOfShots").value(2l))
                .andExpect(jsonPath("$[0].availableDoses").value(9000l));
    }

    @Test
    void save() throws Exception {
        when(vaccineService.save(vaccineCommand)).thenReturn(Optional.ofNullable(vaccineDTO));

        this.mockMvc.perform(
                post("/vaccine")
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(vaccineCommand))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.researchName").value("testResearchName"))
                .andExpect(jsonPath("$.manufacturerName").value("testManufacturerName"))
                .andExpect(jsonPath("$.type").value("testType"))
                .andExpect(jsonPath("$.numberOfShots").value(2l))
                .andExpect(jsonPath("$.availableDoses").value(9000l));
    }

    @Test
    void delete() throws Exception {

        this.mockMvc.perform(
                MockMvcRequestBuilders.delete("/vaccine/{researchName}", "testResearchName")
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(csrf())
        )
                .andExpect(status().isNoContent());

        verify(vaccineService).delete("testResearchName");
    }

    @Test
    void update() throws Exception {
        VaccineCommand vaccineCommand = new VaccineCommand(1L, "testResearchName", "testManufacturerName", "testType", 2L);
        when(vaccineService.update("testResearchName", vaccineCommand)).thenReturn(
                Optional.ofNullable(vaccineDTO));

        this.mockMvc.perform(
                put("/vaccine/{researchName}", "testResearchName")
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(
                                user("newUser").password("newUser").roles("UPDATER")
                        )
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(vaccineCommand))
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    void findAllByAvailableDose() throws Exception {
        when(vaccineService.findAllByAvailableDose(42000, 90000)).thenReturn(
                new ArrayList<>(Arrays.asList(vaccineDTO)));

        this.mockMvc.perform(
                get("/vaccine")
                        .param("lowerLimit", String.valueOf(42000l))
                        .param("upperLimit", String.valueOf(90000l))
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(
                                user("newUser").password("newUser").roles("UPDATER")
                        )
                        .with(csrf())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].researchName").value("testResearchName"))
                .andExpect(jsonPath("$[0].manufacturerName").value("testManufacturerName"))
                .andExpect(jsonPath("$[0].type").value("testType"))
                .andExpect(jsonPath("$[0].numberOfShots").value(2l))
                .andExpect(jsonPath("$[0].availableDoses").value(9000l));
    }

    @Test
    void getById() throws Exception {
        when(vaccineService.findById(1l)).thenReturn(Optional.ofNullable(vaccineDTO));

        this.mockMvc.perform(
                get("/vaccine/id/{id}", 1l)
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(
                                user("newUser").password("newUser").roles("UPDATER")
                        )
                        .with(csrf())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.researchName").value("testResearchName"))
                .andExpect(jsonPath("$.manufacturerName").value("testManufacturerName"))
                .andExpect(jsonPath("$.type").value("testType"))
                .andExpect(jsonPath("$.numberOfShots").value(2l))
                .andExpect(jsonPath("$.availableDoses").value(9000l));
    }
}