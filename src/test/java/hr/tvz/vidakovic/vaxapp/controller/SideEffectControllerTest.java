package hr.tvz.vidakovic.vaxapp.controller;

import hr.tvz.vidakovic.vaxapp.service.SideEffectService;
import hr.tvz.vidakovic.vaxapp.transfer.SideEffectDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class SideEffectControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    SideEffectService sideEffectService;

    @Test
    void getAll() throws Exception {
        when(sideEffectService.findAll()).thenReturn(
                new ArrayList<>(Arrays.asList(
                        new SideEffectDTO("testDescription", "testLongDescription", 10)
                ))
        );

        this.mockMvc.perform(
                get("/side-effect")
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(csrf())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].shortDescription").value("testDescription"))
                .andExpect(jsonPath("$[0].description").value("testLongDescription"))
                .andExpect(jsonPath("$[0].frequency").value(10));
    }

    @Test
    void getByResearchName() throws Exception {
        when(sideEffectService.findByResearchName(anyString())).thenReturn(
                new ArrayList<>(Arrays.asList(
                        new SideEffectDTO("testDescription", "testLongDescription", 10)
                ))
        );

        this.mockMvc.perform(
                get("/side-effect")
                        .param("vaccineResearchName", "testResearchName")
                        .with(
                                user("user").password("user").roles("USER")
                        )
                        .with(
                                user("admin").password("admin").roles("ADMIN")
                        )
                        .with(csrf())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].shortDescription").value("testDescription"))
                .andExpect(jsonPath("$[0].description").value("testLongDescription"))
                .andExpect(jsonPath("$[0].frequency").value(10));
    }

    @Test
    void getByShortDescription() throws Exception {
        when(sideEffectService.findSideEffectByShortDescription(anyString())).thenReturn(
                Optional.of(new SideEffectDTO("testDescription", "testLongDescription", 10)));

        this.mockMvc.perform(
                get("/side-effect/get/{shortDescription}", "testDescription"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void findByLongDescription() throws Exception {
        when(sideEffectService.findByLongDescription(anyString())).thenReturn(
                new ArrayList<>(Arrays.asList(
                        new SideEffectDTO("testDescription", "testLongDescription", 10))));

        this.mockMvc.perform(
                get("/side-effect/search/{longDescription}", "testLongDescription"))
                .andExpect(status().isUnauthorized());
    }
}