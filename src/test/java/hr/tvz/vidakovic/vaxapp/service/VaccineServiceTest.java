package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.command.VaccineCommand;
import hr.tvz.vidakovic.vaxapp.domain.Vaccine;
import hr.tvz.vidakovic.vaxapp.repository.VaccineRepository;
import hr.tvz.vidakovic.vaxapp.transfer.VaccineDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class VaccineServiceTest {

    @Autowired
    VaccineService vaccineService;

    @MockBean
    VaccineRepository vaccineRepository;

    VaccineCommand vaccineCommand = new VaccineCommand(1L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L);

    @Test
    void findAll() {
        when(vaccineRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(
                new Vaccine(1L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L))));

        List<VaccineDTO> vaccineDTOS = vaccineService.findAll();
        assertNotNull(vaccineDTOS);
        assertEquals(vaccineDTOS.size(), 1);
        assertEquals(vaccineDTOS.get(0).getResearchName(), "testResearchName");
    }

    @Test
    void findVaccineByResearchName() {
        when(vaccineRepository.findVaccineByResearchName("testResearchName")).thenReturn(
                Optional.of(new Vaccine(1L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L)));

        Optional<VaccineDTO> vaccineDTO = vaccineService.findVaccineByResearchName("testResearchName");
        assertNotNull(vaccineDTO);
        assertEquals(vaccineDTO.get().getResearchName(), "testResearchName");
        assertEquals(vaccineDTO.get().getManufacturerName(), "testManufacturerName");
    }

    @Test
    void findVaccinesByRequiredDose() {
        when(vaccineRepository.findVaccinesByRequiredDose(2L)).thenReturn(new ArrayList<>(Arrays.asList(
                new Vaccine(1L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L))));

        List<VaccineDTO> vaccineDTOS = vaccineService.findVaccinesByRequiredDose(2L);
        assertNotNull(vaccineDTOS);
        assertEquals(vaccineDTOS.size(), 1);
        assertEquals(vaccineDTOS.get(0).getResearchName(), "testResearchName");
    }

    @Test
    void save() {
        when(vaccineRepository.save(vaccineCommand)).thenReturn(
                Optional.of(new Vaccine(1L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L)));

        Optional<VaccineDTO> vaccineDTO = vaccineService.save(vaccineCommand);

        assertNotNull(vaccineDTO);
        assertEquals(vaccineDTO.get().getResearchName(), "testResearchName");
        assertNotEquals(vaccineDTO.get().getNumberOfShots(), 3L);
    }

    @Test
    void delete() {
        vaccineService.delete("AZD1222");
        verify(vaccineRepository).delete("AZD1222");
    }

    @Test
    void update() {
        when(vaccineRepository.update("testResearchName", vaccineCommand)).thenReturn(
                Optional.of(new Vaccine(1L, "testResearchName", "testManufacturerName", "testType", 2L, 9000L)));

        Optional<VaccineDTO> vaccineDTO = vaccineService.update("testResearchName", vaccineCommand);
        assertNotNull(vaccineDTO);
        assertEquals(vaccineDTO.get().getResearchName(), "testResearchName");
        assertNotEquals(vaccineDTO.get().getNumberOfShots(), 3L);
        

    }

    @Test
    void findAllByAvailableDose() {
        when(vaccineRepository.findAllByAvailableDose(2, 42000)).thenReturn(
                new ArrayList<>(Arrays.asList(
                        new Vaccine(1L, "testResearchName", "testManufacturerName", "testType", 2L, 4000L))));

        List<VaccineDTO> vaccineDTOS = vaccineService.findAllByAvailableDose(2, 42000);
        assertFalse(vaccineDTOS.isEmpty());
        assertEquals(vaccineDTOS.get(0).getResearchName(), "testResearchName");
        assertNotEquals(vaccineDTOS.get(0).getNumberOfShots(), 3L);
    }

    @Test
    void findById() {
        when(vaccineRepository.findById(1l)).thenReturn(
                Optional.of(new Vaccine(1L, "testResearchName", "testManufacturerName", "testType", 2L, 4000L)));
        Optional<VaccineDTO> vaccineDTO = vaccineService.findById(1l);
        assertNotNull(vaccineDTO);
        assertEquals(vaccineDTO.get().getResearchName(), "testResearchName");
    }
}