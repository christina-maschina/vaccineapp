package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.domain.SideEffect;
import hr.tvz.vidakovic.vaxapp.repository.SideEffectJpaRepository;
import hr.tvz.vidakovic.vaxapp.transfer.SideEffectDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
class SideEffectServiceTest {
    @Autowired
    SideEffectService sideEffectService;

    @MockBean
    SideEffectJpaRepository sideEffectJpaRepository;

    @Test
    void findAll() {
        when(sideEffectJpaRepository.findAll()).thenReturn(
                new ArrayList<>(Arrays.asList(
                        new SideEffect(1L, "testDescription", "testLongDescription", 10)
                ))
        );
        List<SideEffectDTO> sideEffectDTOS = sideEffectService.findAll();
        assertNotNull(sideEffectDTOS);
        assertEquals(sideEffectDTOS.size(), 1);
        assertNotEquals(sideEffectDTOS.get(0).getFrequency(), 9);
    }

    @Test
    void findSideEffectByShortDescription() {
        when(sideEffectJpaRepository.findByShortDescription(anyString())).thenReturn(
                Optional.of(new SideEffect(1L, "testShortDescription", "testLongDescription", 10)));

        Optional<SideEffectDTO> sideEffectDTO = sideEffectService.findSideEffectByShortDescription("testShortDescription");
        assertNotNull(sideEffectDTO);
        assertEquals(sideEffectDTO.get().getShortDescription(), "testShortDescription");
    }

    @Test
    void findByResearchName() {
        when(sideEffectJpaRepository.findByVaccine_ResearchName(anyString())).thenReturn(
                new ArrayList<>(Arrays.asList(
                        new SideEffect(1L, "testShortDescription", "testLongDescription", 10))));

        List<SideEffectDTO> sideEffectDTOS = sideEffectService.findByResearchName("testResearchName");
        assertNotNull(sideEffectDTOS);
        assertEquals(sideEffectDTOS.size(), 1);
    }

    @Test
    void findByLongDescription() {
        when(sideEffectJpaRepository.findByDescriptionLike(anyString())).thenReturn(
                new ArrayList<>(Arrays.asList(
                        new SideEffect(1L, "testShortDescription", "testLongDescription", 10))));

        List<SideEffectDTO> sideEffectDTOS = sideEffectService.findByLongDescription("%test%");
        assertNotNull(sideEffectDTOS);
        assertEquals(sideEffectDTOS.size(), 1);
    }

}