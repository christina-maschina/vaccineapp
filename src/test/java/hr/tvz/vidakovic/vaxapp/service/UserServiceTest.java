package hr.tvz.vidakovic.vaxapp.service;

import hr.tvz.vidakovic.vaxapp.domain.User;
import hr.tvz.vidakovic.vaxapp.repository.UserRepository;
import hr.tvz.vidakovic.vaxapp.transfer.UserDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserServiceTest {

    @Autowired
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @Test
    void findByUsername() {
        when(userRepository.findOneByUsername("user")).thenReturn(
                Optional.of(new User(1L, "user", "user", "Kristina", "Vidakovic")));

        Optional<UserDTO> userDTO = userService.findByUsername("user");
        assertNotNull(userDTO);
        assertEquals(userDTO.get().getUsername(), "user");
    }
}